-- vim: softtabstop=2:tabstop=2:shiftwidth=2:expandtab:retab
-- alphabet - Transliteration module for LLP "CB PROMSVYAZ"
-- Author: Alexandr Mikhailenko (a.k.a) FlashHacker
-- Date: 5.11.14
-- 

--
-- Global array with the alphabet in two languages
--
alphabet={ ["А"] = "A", ["а"] = "a", ["Б"] = "B",  ["б"] = "b",
["В"] = "V", ["в"] = "v", ["Г"] = "G", ["г"] = "g",  ["Д"] = "D",
["д"] = "d", ["Е"] = "e", ["е"] = "e", ["Ё"] = "JO", ["ё"] = "jo",
["Ж"] = "ZH",["ж"] = "zh",["З"] = "Z", ["з"] = "z",  ["И"] = "I",
["и"] = "i", ["Й"] = "J", ["й"] = "j", ["К"] = "K",  ["к"] = "k",
["Л"] = "L", ["л"] = "l", ["М"] = "M", ["м"] = "m",  ["Н"] = "N",
["н"] = "n", ["О"] = "O", ["о"] = "o", ["П"] = "P",  ["п"] = "p",
["Р"] = "r", ["р"] = "r", ["С"] = "s", ["с"] = "s",  ["Т"] = "t",
["т"] = "t", ["У"] = "u", ["у"] = "u", ["Ф"] = "f",  ["ф"] = "f",
["Х"] = "H", ["х"] = "h", ["Ц"] = "C", ["ц"] = "c",  ["Ч"] = "CH",
["ч"] = "ch",["Ш"] = "SH",["ш"] = "sh",["Щ"] = "SHH",["щ"] = "shh",
["Ъ"] = "#", ["ъ"] = "#", ["Ы"] = "Y", ["ы"] = "y",  ["Ь"] = "'",
["ь"] = "'", ["Э"] = "JE",["э"] = "je",["Ю"] = "JU", ["ю"] = "ju",
["Я"] = "JA",["я"] = "ja" }

--
-- Making font uppercase
-- 
function alphabet.RusUp(Ch)
  if string.byte(Ch) < 224 then return string.upper(Ch) end
  return string.char(string.byte(Ch)-32)
end

--
-- Produce transliteration of text
-- 
function alphabet.translit(s)
  if string.len(s) == 0 then return end
  local pos = 1
  local outstr = ""
  local res, toFind
  while (pos <= string.len(s)) do
    local isCapital = true
	if string.sub(s, pos, pos) == string.lower(string.sub(s, pos, pos)) then isCapital = false end
	for i = 3, 1, -1 do
	  toFind = string.lower(string.sub(s, pos, pos + i - 1))
	  res = alphabet[toFind]
	  if res ~= nil then
		if isCapital then res = alphabet.RusUp(res) end 
		outstr = outstr..res pos = pos + string.len(toFind) 
		break 
	  end
	end
	if res == nil then outstr = outstr..toFind pos = pos + 1 end
  end
  return outstr
end

--
-- Since this module give values
--
return alphabet
--
-- Test transliteration module (Uncomment to check work)
--
--print(alphabet.translit("Транслиты — русский, руссодойч, иврит, армянский, украинский, белорусский, греческий, грузинский, литовский, таджикский, казахский"))