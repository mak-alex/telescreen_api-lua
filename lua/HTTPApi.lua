--[[

 #####                ######                           #     # ####### ####### ######       #    ######  ###    #####                               
#     # #    # #####  #     # #    # # #      #####    #     #    #       #    #     #     # #   #     #  #    #     # #      # ###### #    # ##### 
#       ##  ## #    # #     # #    # # #      #    #   #     #    #       #    #     #    #   #  #     #  #    #       #      # #      ##   #   #   
#       # ## # #    # ######  #    # # #      #    #   #######    #       #    ######    #     # ######   #    #       #      # #####  # #  #   #   
#       #    # #    # #     # #    # # #      #    #   #     #    #       #    #         ####### #        #    #       #      # #      #  # #   #   
#     # #    # #    # #     # #    # # #      #    #   #     #    #       #    #         #     # #        #    #     # #      # #      #   ##   #   
 #####  #    # #####  ######   ####  # ###### #####    #     #    #       #    #         #     # #       ###    #####  ###### # ###### #    #   #   
 

888                    888     888    .d8888b.                    888                        
888                    888     888   d88P  Y88b                   888                        
888                    888     888   Y88b.                        888                        
88888b.  888  888      Y88b   d88P    "Y888b.   888  888  .d8888b 88888b.   .d88b.  888  888 
888 "88b 888  888       Y88b d88P        "Y88b. 888  888 d88P"    888 "88b d8P  Y8b 888  888 
888  888 888  888        Y88o88P           "888 888  888 888      888  888 88888888 Y88  88P 
888 d88P Y88b 888         Y888P  d8b Y88b  d88P Y88b 888 Y88b.    888  888 Y8b.      Y8bd8P  
88888P"   "Y88888          Y8P   Y8P  "Y8888P"   "Y88888  "Y8888P 888  888  "Y8888    Y88P   
              888                                    888                                     
         Y8b d88P                               Y8b d88P                                     
          "Y88P"                                 "Y88P"                                      
	
]]--
package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua;../?.lua";
local  VERSION = 20142807.170200
local  HttpApi = {
       VERSION = VERSION,
       AuthKey = "",
       Authorized = false,
       ServerAddress = "",
       ServerPort = ""       
}

local MCASTMUX_HOME=os.getenv("MCASTMUX_HOME");
local lscript_path=MCASTMUX_HOME.."/lua"

local socket = require("socket")
local ltn12 = require("ltn12")
local http = require("socket.http")
local url = require("socket.url")
local JSON = assert(loadfile(lscript_path .. "/JSON.lua"))()

--[[

8888888b.                    888                                       8888888          888                                       888          
888   Y88b                   888                                         888            888                                       888          
888    888                   888                                         888            888                                       888          
888   d88P  8888b.   .d8888b 888  888  8888b.   .d88b.   .d88b.          888   88888b.  888888  .d88b.  888d888 88888b.   8888b.  888 .d8888b  
8888888P"      "88b d88P"    888 .88P     "88b d88P"88b d8P  Y8b         888   888 "88b 888    d8P  Y8b 888P"   888 "88b     "88b 888 88K      
888        .d888888 888      888888K  .d888888 888  888 88888888         888   888  888 888    88888888 888     888  888 .d888888 888 "Y8888b. 
888        888  888 Y88b.    888 "88b 888  888 Y88b 888 Y8b.             888   888  888 Y88b.  Y8b.     888     888  888 888  888 888      X88 
888        "Y888888  "Y8888P 888  888 "Y888888  "Y88888  "Y8888        8888888 888  888  "Y888  "Y8888  888     888  888 "Y888888 888  88888P' 
                                                    888                                                                                        
                                               Y8b d88P                                                                                        
                                                "Y88P"                                                                                         

]]--

local function joinTheResponse(text_array)
    local ret=""
    for i = 1,#text_array
    do
      ret=ret..text_array[i];
     end
     return ret;
end

local function url_decode(str)
  str = string.gsub (str, "+", " ")
  str = string.gsub (str, "%%(%x%x)",
  function(h) return string.char(tonumber(h,16)) end)
  str = string.gsub (str, "\r\n", "\n")
 return str
end

local function url_encode(str)
  if (str) then
    str = string.gsub (str, "\n", "\r\n")
    str = string.gsub (str, "([^%w %-%_%.%~])",
    function (c) return string.format ("%%%02X", string.byte(c)) end)
    str = string.gsub (str, " ", "+")
  end
  return str	
end

local function tableToPayload(tbl)
    local res = "";
    for param,value in pairs(tbl)
    do
	res = res .. url_encode(param) .. "=" .. url_encode(value) .. "&"	
    end;
    return res;
end;

local function apiRequest(scriptUri,reqbody,reqMethod)
    local respbody = {}
    if HttpApi["Authorized"] == false
    then
	return nil
    end;
    --print(reqbody);
    local rbody = reqbody
    local  body, code, headers, status = http.request {
            method = reqMethod,
            url = scriptUri,
            source = ltn12.source.string(rbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(rbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local result = JSON:decode(joinTheResponse(respbody))
        if result["success"] == true
        then
    	    --print(joinTheResponse(result))
    	    return JSON:decode(joinTheResponse(respbody))
    	else
    	    print(joinTheResponse(respbody))
    	    return nil
    	end;        
end;

--[[

 .d8888b.                             d8b                          .d8888b.                    888                     888 
d88P  Y88b                            Y8P                         d88P  Y88b                   888                     888 
Y88b.                                                             888    888                   888                     888 
 "Y888b.    .d88b.  .d8888b  .d8888b  888  .d88b.  88888b.        888         .d88b.  88888b.  888888 888d888  .d88b.  888 
    "Y88b. d8P  Y8b 88K      88K      888 d88""88b 888 "88b       888        d88""88b 888 "88b 888    888P"   d88""88b 888 
      "888 88888888 "Y8888b. "Y8888b. 888 888  888 888  888       888    888 888  888 888  888 888    888     888  888 888 
Y88b  d88P Y8b.          X88      X88 888 Y88..88P 888  888       Y88b  d88P Y88..88P 888  888 Y88b.  888     Y88..88P 888 
 "Y8888P"   "Y8888   88888P'  88888P' 888  "Y88P"  888  888        "Y8888P"   "Y88P"  888  888  "Y888 888      "Y88P"  888 
                                                                                                                           
                                                                                                                           
                                                                                                                           
    
]]--

--[[ 
    NAME:  openApiConnection(serverAddress,userName,password)
    SUMMARY: 
	    Creates a new session on CmdBuild Host with specified username and password.
    ARGUMENTS:
	       serverAddress -- Address of target server where we want to connect and open session
	       userName      -- Username to login at the server
	       password      -- Password for this login
    RETURN VALUE:
		0         -- if connection was successful
		1         -- if connection failed for some reasons ( no reason code returtned due to failure) 	       	    
]]--   

function openApiConnection(serverAddress,userName,password)
    local loginScriptUri = "http://" .. serverAddress .. "/cmdbuild/services/json/login/login"
    local reqbody = "ext-comp-1009-inputEl=ru&username="..userName.."&password="..password
    local respbody = {}
    local authResult = {}
    
    local  body, code, headers, status = http.request {
            method = "POST",
            url = loginScriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8 ",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-"..VERSION,
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
	authResult = JSON:decode(joinTheResponse(respbody))        
	if authResult["success"] == true
	then
    	    HttpApi["AuthKey"] = headers["set-cookie"]
            HttpApi["Authorized"] = true
            HttpApi["ServerAddress"] = serverAddress
            return 0;
	else
	    HttpApi["Authorized"] = false;
	    return 1;
	end    
end

--[[ 
    NAME:  closeApiConnection()
    SUMMARY: 
	    Disposes session on CmdBuild
    ARGUMENTS:
	    NONE
    RETURN VALUE:
		0         -- if connection was successful
		1         -- if connection failed for some reasons ( no reason code returtned due to failure) 	       	    
]]--   


function closeApiConnection()
    local loginScriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/logout.jsp"
    local reqbody = "EMPTY-REQUEST-BODY"
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return 1;
    end;	
    local  body, code, headers, status = http.request {
            method = "GET",
            url = loginScriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-"..VERSION,
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
       HttpApi["AuthKey"] = "";
       HttpApi["Authorized"] = false;	    
       return 0;
end

--[[
     NAME: getClassId(className)
     SUMMARY:
    	   Returns class id from CmdBuild by its name
     ARGUMENTS:
           className :: STRING
     RETURN VALUE:
           -1 if error was occured during network operation with CmdBuild
           > 0 if successfull, returned value is classId for className
     REMARKS:
           Function is private for this package
                      	   
]]--

local function getClassId(className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/schema/modclass/getallclasses"
    local reqbody = ""
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return -1;
    end;	
    local  body, code, headers, status = http.request {
            method = "GET",
            url = scriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local response = JSON:decode(joinTheResponse(respbody))
        if response["success"] == true
        then
    	    local classes = response["classes"]
    	    for i=1, table.getn(classes)
    	    do
    		if classes[i].name == className
    		then
    		    return classes[i].id
    		end;
    	    end;
    	else
    	    return -1
    	end        
	return -1
end;

--[[

8888888b.           888                    .d8888b.                        888        .d88888b.                                     888    d8b                            
888  "Y88b          888                   d88P  Y88b                       888       d88P" "Y88b                                    888    Y8P                            
888    888          888                   888    888                       888       888     888                                    888                                   
888    888  8888b.  888888  8888b.        888         8888b.  888d888  .d88888       888     888 88888b.   .d88b.  888d888  8888b.  888888 888  .d88b.  88888b.  .d8888b  
888    888     "88b 888        "88b       888            "88b 888P"   d88" 888       888     888 888 "88b d8P  Y8b 888P"       "88b 888    888 d88""88b 888 "88b 88K      
888    888 .d888888 888    .d888888       888    888 .d888888 888     888  888       888     888 888  888 88888888 888     .d888888 888    888 888  888 888  888 "Y8888b. 
888  .d88P 888  888 Y88b.  888  888       Y88b  d88P 888  888 888     Y88b 888       Y88b. .d88P 888 d88P Y8b.     888     888  888 Y88b.  888 Y88..88P 888  888      X88 
8888888P"  "Y888888  "Y888 "Y888888        "Y8888P"  "Y888888 888      "Y88888        "Y88888P"  88888P"   "Y8888  888     "Y888888  "Y888 888  "Y88P"  888  888  88888P' 
                                                                                                 888                                                                      
                                                                                                 888                                                                      
                                                                                                 888                                                                      
    
]]--                

--[[
     NAME: createCard(cardData, className)
     SUMMARY:
    	   Creates card for class of specicifed type
     ARGUMENTS:
           className :: STRING
           cardData  :: OBJECT 
     RETURN VALUE:
           -1 if error was occured during network operation with CmdBuild
           > 0 if successfull, returned value is cardId for newly created card
]]--

function createCard(cardData,className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/management/modcard/updatecard"
    cardData.className = className
    cardData.cardId = -1
    local reqbody = tableToPayload(cardData)
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return -1;
    end;	
    local  body, code, headers, status = http.request {
            method = "POST",
            url = scriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local response = JSON:decode(joinTheResponse(respbody))
        if response["success"] == true
        then
    	    return response["id"]
    	else
    	    print(reqbody)
    	    print(joinTheResponse(respbody))
    	    return -1
    	end        
end;

--[[
     NAME: alterCard(cardId, cardData, className)
     SUMMARY:
    	   Modifies data for specified card in CmdBuild
     ARGUMENTS:
           cardId    :: INTEGER
           className :: STRING
           cardData  :: OBJECT 
     RETURN VALUE:
           -1 if error was occured during network operation with CmdBuild
           > 0 if successfull, returned value is cardId for modified card
]]--


function alterCard(cardId, cardData, className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/management/modcard/updatecard"
    cardData.className = className
    cardData.cardId = cardId
    local reqbody = tableToPayload(cardData)
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return -1;
    end;	
    local  body, code, headers, status = http.request {
            method = "POST",
            url = scriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local response = JSON:decode(joinTheResponse(respbody))
        if response["success"] == true
        then
    	    return response["id"]
    	else
    	    return -1
    	end        
end;

--[[
     NAME: dropCard(cardId, className)
     SUMMARY:
    	   Removes card with specified id from database
     ARGUMENTS:
           className :: STRING
           cardId  :: INTEGER
     RETURN VALUE:
           1 if error was occured during network operation with CmdBuild
           0 if successfull deletetion was performed 
]]--


function dropCard(cardId,className)
    local classId = getClassId(className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/management/modcard/deletecard"
    local reqbody = "IdClass="..classId.."&Id="..cardId
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return 1;
    end;	
    local  body, code, headers, status = http.request {
            method = "POST",
            url = scriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local response = JSON:decode(joinTheResponse(respbody))
        if response["success"] == true
        then
    	    return 0
    	else
    	    return 1
    	end        
end;

--[[
     NAME: showCards(className)
     SUMMARY:
    	   Removes card with specified id from database
     ARGUMENTS:
           className :: STRING
     RETURN VALUE:
           nil if error was occured during network operation with CmdBuild
           non nil array of objects if operation was completted successfully
]]--


function showCards(className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/management/modcard/getcardlist?className="..className
    local reqbody = "--EMPTY--"
    local respbody = {}
    local authResult = {}
    if HttpApi["Authorized"] == false
    then
	return nil;
    end;	
    local  body, code, headers, status = http.request {
            method = "GET",
            url = scriptUri,
            source = ltn12.source.string(reqbody),
            headers = 
                        {
                            ["Accept"] = "*/*",
                            ["Accept-Encoding"] = "gzip, deflate",
                            ["Accept-Language"] = "ru-ru, en-us",
                            ["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8",
                            ["User-Agent"] = "Lua/Kbps-CmdBuild-Http-Api-" .. VERSION,
                            ["Cookie"] = HttpApi["AuthKey"],
                            ["Content-Length"] = string.len(reqbody)
                        },        
            sink = ltn12.sink.table(respbody)   
        }
        local response = JSON:decode(joinTheResponse(respbody))
        if response["success"] == true
        then
	    local rows = response["rows"]
	    return rows	    
    	else
    	    return nil
    	end        
        return nil
end;

--[[
     NAME: showCards(className)
     SUMMARY:
    	   Removes card with specified id from database
     ARGUMENTS:
           cardId    :: INTEGER
           className :: STRING
     RETURN VALUE:
           nil if error was occured during network operation with CmdBuild
           non nil object filled with requested card data if operation was successfull
]]--


function showCard(cardId,className)
    local cards = showCards(className)
    if cards == nil then
	return nil;
    end;
    for i=1, table.getn(cards)
    do
	if cards[i].Id == cardId 
	then
	    return cards[i];
	end    
    end;
    return nil;	
end;

--[[

 .d8888b.  888                                   .d88888b.                                     888    d8b                            
d88P  Y88b 888                                  d88P" "Y88b                                    888    Y8P                            
888    888 888                                  888     888                                    888                                   
888        888  8888b.  .d8888b  .d8888b        888     888 88888b.   .d88b.  888d888  8888b.  888888 888  .d88b.  88888b.  .d8888b  
888        888     "88b 88K      88K            888     888 888 "88b d8P  Y8b 888P"       "88b 888    888 d88""88b 888 "88b 88K      
888    888 888 .d888888 "Y8888b. "Y8888b.       888     888 888  888 88888888 888     .d888888 888    888 888  888 888  888 "Y8888b. 
Y88b  d88P 888 888  888      X88      X88       Y88b. .d88P 888 d88P Y8b.     888     888  888 Y88b.  888 Y88..88P 888  888      X88 
 "Y8888P"  888 "Y888888  88888P'  88888P'        "Y88888P"  88888P"   "Y8888  888     "Y888888  "Y888 888  "Y88P"  888  888  88888P' 
                                                            888                                                                      
                                                            888                                                                      
                                                            888                                                                      

]]--

--[[
     NAME: createClass(className, classParams)
     SUMMARY:
    	   Creates a new instance of class with specified name and parameters
     ARGUMENTS:
           className :: STRING
           classParams :: OBJECT
     RETURN VALUE:
	    -1 if error was occured during class creation
	    >0 value if class was created successfully, return value will be an identifier of a newly created class
    
]]--

function createClass(className,classParams)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/schema/modclass/savetable"
    local parentClassId = getClassId(classParams["parentClass"])
    local inherittedClassId = getClassId(classParams["inherittedClass"])
    local creationParamString = "name="..url_encode(className).."&text="..url_encode(className).."&tableType="..url_encode(classParams["classType"]).."&parent="..parentClassId.."&superclass="..url_encode(classParams["isSuperClass"]).."&active="..url_encode(classParams["isActive"]).."&forceCreation="..url_encode(classParams["forceCreation"]).."&isprocess="..url_encode(classParams["isProcess"]).."&description="..url_encode(classParams["classDescription"]).."&inherits="..inherittedClassId
    local result = apiRequest(scriptUri,creationParamString,"POST");
    if result ~= nil then
	return result["table"]["id"]
    else
	return -1
    end;
    return -1		
end;

function alterClass(classId, classData)
end;

--[[
     NAME: dropClass(className)
     SUMMARY:
    	   Removes instance of class with specified name and parameters
     ARGUMENTS:
           className :: STRING
     RETURN VALUE:
	    1 if error was occured during class removal
	    0 if class was removed successfully
    
]]--

function dropClass(className)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/schema/modclass/deletetable"
    local paramString = "name="..url_encode(className)
    local result = apiRequest(scriptUri,paramString,"POST")
    if result ~= nil then
	return 0
    else
	return 1
    end
end;

--[[
     NAME: showClasses(isActive)
     SUMMARY:
    	   Shows all classes in current system
     ARGUMENTS:
           isActive :: BOOLEAN
     RETURN VALUE:
	    nil value if error was occured during class list retrieval
	    list of classes if operation was completted successfully
    
]]--


function showClasses(isActive)
    local scriptUri = "http://" .. HttpApi["ServerAddress"] .. "/cmdbuild/services/json/schema/modclass/getallclasses?_dc=1406034352572"
    local paramString = ""
    if isActive ~= nil 
    then
        scriptUri = scriptUri ..  "&active="..url_encode(isActive)
    else
	scriptUri = scriptUri ..  "&active=true"
    end	  
    local result = apiRequest(scriptUri,paramString,"GET")
    if result ~= nil
    then
	return result["classes"]
    else
	return nil
    end		
end;

--[[
     NAME: describeClassStructure(className)
     SUMMARY:
    	   Retrieves structure of class specified in className parameter
     ARGUMENTS:
           className :: STRING
     RETURN VALUE:
	    nil value if error was occured during class field list retrieval
	    list of fields for specified class if operation was completted successfully
    
]]--

function describeClass(className)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/getattributelist?_dc=1406035829374&className=123123&page=" .. className
    local paramString = ""
    local result = apiRequest(scriptUri,paramString,"GET")
    if result ~= nil
    then
	return result["attributes"]
    else
	return nil
    end		
end;

--[[
     NAME: addFieldToClass(className,fieldInfo)
     SUMMARY:
    	   Creates new field in the specified class
     ARGUMENTS:
           className :: STRING
           fieldInfo :: OBJECT
     RETURN VALUE:
           0 if operation accomplished successfully and field added to class
           i if operation failed
    
]]--


function addFieldToClass(className, fieldInfo)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/saveattribute"
    fieldInfo["className"] = className
    local paramString = tableToPayload(fieldInfo)
    local result = apiRequest(scriptUri,paramString,"POST")
    if result ~= nil
    then
	return 0
    else
	return 1
    end		
end;

--[[
     NAME: deleteFieldFromClass(className,fieldName)
     SUMMARY:
    	   Removes field from a class 
     ARGUMENTS:
           className :: STRING
           fieldName :: STRING
     RETURN VALUE:
           0 if operation accomplished successfully and field was removed from a class
           i if operation failed
    
]]--

function deleteFieldFromClass(className, fieldName)
    local csriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/deleteattribute"
    local paramString = "className="..url_encode(className).."&name="..url_encode(fieldname)
    local result = apiRequest(scriptUri,paramString,"POST")
    if result ~= nil 
    then
	return 0
    else
	return 1
    end		
end;

--[[
     NAME: modifyFieldInClass(className,fieldName,fieldInfo)
     SUMMARY:
    	   Modifies existing instance of field in a class
     ARGUMENTS:
           className :: STRING
           fieldName :: STRING
           fieldInfo :: OBJECT
     RETURN VALUE:
           0 if operation accomplished successfully
           i if operation failed
    
]]--

function modifyFieldInClass(className, fieldName ,fieldInfo)
    fieldInfo["name"] = fieldName
    return addFieldToClass(classname, fieldInfo)
end;

--[[
8888888b.           .d888                                                                                        d8b                   888          888    d8b                   
888   Y88b         d88P"                                                                                         Y8P                   888          888    Y8P                   
888    888         888                                                                                                                 888          888                          
888   d88P .d88b.  888888 .d88b.  888d888 .d88b.  88888b.   .d8888b .d88b.       88888b.d88b.   8888b.  88888b.  888 88888b.  888  888 888  8888b.  888888 888  .d88b.  88888b.  
8888888P" d8P  Y8b 888   d8P  Y8b 888P"  d8P  Y8b 888 "88b d88P"   d8P  Y8b      888 "888 "88b     "88b 888 "88b 888 888 "88b 888  888 888     "88b 888    888 d88""88b 888 "88b 
888 T88b  88888888 888   88888888 888    88888888 888  888 888     88888888      888  888  888 .d888888 888  888 888 888  888 888  888 888 .d888888 888    888 888  888 888  888 
888  T88b Y8b.     888   Y8b.     888    Y8b.     888  888 Y88b.   Y8b.          888  888  888 888  888 888  888 888 888 d88P Y88b 888 888 888  888 Y88b.  888 Y88..88P 888  888 
888   T88b "Y8888  888    "Y8888  888     "Y8888  888  888  "Y8888P "Y8888       888  888  888 "Y888888 888  888 888 88888P"   "Y88888 888 "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                                                     888                                                         
                                                                                                                     888                                                         
                                                                                                                     888                                                         
]]--
--[[
    refInfo object structure:
    refInfo = 
    {
	name=<Name of the reference>,
	description=<Reference description>,
	idClass1=<Source table name>,
	idClass2=<Linked table name>,
	descr_1=<Direct relation>,
	descr_2=<Backward relation>,
	cardinality=<Relation cardinality>, (may be 1:N, N:1, 1:1, N:N)
	isMasterDetail=false,
	md_label=,
	active=true,
	id=-1
    }
]]--


function createReference(refName, refInfo)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/savedomain"
    refInfo["idClass1"] = getClassId(refInfo["idClass1"])
    refInfo["idClass2"] = getClassId(refInfo["idClass2"])
    local paramString = tableToPayload(refInfo)
    local result = apiRequest(scriptUri, paramString,"POST")
    if result ~= nil
    then
	return result["domain"]["idDomain"]
    else
	return -1
    end;
end;

function dropReference(refName)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/deletedomain"
    local paramString = "domainName="..url_encode(refname)
    local result = apiRequest(scriptUri, paramString,"POST")
    if result ~= nil
    then
	return 0
    else
	return 1
    end;
end;

function listReferencesFor(className)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modclass/getdomainlist?_dc=1406191315220&className="..url_encode(className)
    local paramString = "";
    local result = apiRequest(scriptUri,paramString,"GET")
    if result ~= nil
    then
	return result["domains"]
    else
	return nil
    end;
end;

function getReferenceValue(className, refFieldName,refFieldValue)
    local fieldsForClass = describeClass(className)
    local referenceName = ""
    for i=0,table.getn(fieldsForClass)
    do
	if ((fieldsForClass[i]["name"] == refFieldName) and (fieldsForClass[i]["type"] == "REFERENCE"))
	then
	    referenceName = fieldsForClass[i]["referencedClassName"]
	    break;
	end
    end;
    local refValues = showCards(referenceName)
    for i=0,table.getn(refValues)
    do
	if(refValues[i]["Description"] == refFieldValue)
	then
	    return refValues[i]
	end
    end
    return nil	
end;

--[[

888                       888                             888                                                                       888                    888 
888                       888                             888                                                                       888                    888 
888                       888                             888                                                                       888                    888 
888      .d88b.   .d88b.  888  888 888  888 88888b.       888888 888  888 88888b.   .d88b.  .d8888b        .d8888b .d88b.  88888b.  888888 888d888 .d88b.  888 
888     d88""88b d88""88b 888 .88P 888  888 888 "88b      888    888  888 888 "88b d8P  Y8b 88K           d88P"   d88""88b 888 "88b 888    888P"  d88""88b 888 
888     888  888 888  888 888888K  888  888 888  888      888    888  888 888  888 88888888 "Y8888b.      888     888  888 888  888 888    888    888  888 888 
888     Y88..88P Y88..88P 888 "88b Y88b 888 888 d88P      Y88b.  Y88b 888 888 d88P Y8b.          X88      Y88b.   Y88..88P 888  888 Y88b.  888    Y88..88P 888 
88888888 "Y88P"   "Y88P"  888  888  "Y88888 88888P"        "Y888  "Y88888 88888P"   "Y8888   88888P'       "Y8888P "Y88P"  888  888  "Y888 888     "Y88P"  888 
                                            888                       888 888                                                                                  
                                            888                  Y8b d88P 888                                                                                  
                                            888                   "Y88P"  888                                                                                  

]]--

function createLookup(lookupName, parent, orig_type)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modlookup/savelookuptype"
    local paramString = "parent="..url_encode(parent).."&description="..url_encode(lookupName).."&orig_type="..url_encode(orig_type)
    local request = apiRequest(scriptUri,paramString,"POST")
    if result ~= nil
    then
	return 0
    else
	return 1
    end;
end;

function addLookupElement(lookupName,elementInfo)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modlookup/savelookup";
    local paramString = tableToPayload(elementInfo);
    local request = apiRequest(scriptUri,paramString,"POST")
    if result ~= nil
    then
	return result["lookup"]["id"]
    else
	return -1
    end;
end;

function getLookupItems(lookupName)
    local scriptUri = "http://"..HttpApi["ServerAddress"].."/cmdbuild/services/json/schema/modlookup/getlookuplist?type="..lookupName;
    local paramString = ""
    local result = apiRequest(scriptUri,paramString,"GET")
    if result ~= nil
    then
	return result.rows
    else
	return nil
    end;
end;