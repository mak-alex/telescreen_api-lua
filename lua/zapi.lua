--- A Zabbix JSON RPC API wrapper module
-- @module zapi
-- @author  Pavel Kraynyukhov
-- @copyright Design Bureau of Industrial Communications LTD, 2014. All rights reserved.
-- @release $Id: 20339cbe5693067a9ef8a8b1f8f06cd4ca37d88b $
-- vim: ts=2 tabstop=2 shiftwidth=2 expandtab
-- vim: retab 

package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";

local MCASTMUX_HOME=os.getenv("MCASTMUX_HOME");

local lscript_path=MCASTMUX_HOME.."/lua"

local socket = require("socket")
local ltn12 = require("ltn12")
local http = require("socket.http")
local url = require("socket.url")

local JSONf = assert(loadfile(lscript_path.."/JSON.lua"));

local JSON=JSONf()

local zapi={}
zapi.socket=socket;
zapi.ltn12=ltn12;
zapi.http=http;
zapi.url=url;
zapi.loggedin=false
zapi.dotrace=false
zapi.extendout=true

--- Toggle tracing
-- The tracing is off by default
-- @returns nothing
function zapi.tracetoggle()
  zapi.dotrace = not zapi.dotrace
end

--- Prints the string to standard output
--  if dotrace is on
--  @param str is a string to print
function zapi.trace(...)
  if zapi.dotrace
  then
    print(unpack(arg))
  end
end

--- Toggles the extended output of the Zabbix JSON RPC API
-- The extended output is on by default
function zapi.extendouttoggle()
    zapi.extendout = not zapi.extendout
end

--- The login function authenticates the module with the
-- JSON RPC API of the Zabbix server. This function should be
-- called once, before any other functions of the API will be called too
-- This function saves the URL, username, password and id
-- within the module variables, for later reuse
-- @param base_url is the URL to Zabbix JSON RPC server
-- @param user  is the user name
-- @param password  is the password
-- @param id is the session id
function zapi.login(base_url,user,password,id)
  if not base_url
  then
    error("zapi.login(), - base_url is a mandatory argument")
    return nil
  end

  if not id
  then
    id=1
  end

  zapi.base_url=base_url;
  zapi.user=user;
  zapi.password=password;

  local login= [[
  {
    "jsonrpc":"2.0",
    "method":"user.authenticate",
    "params":{
      "user":"]]..user..[[",
      "password":"]]..password..[["
    },
    "id": ]]..id..[[
  }
  ]]
   
  local resp=httpPOST(zapi.base_url,login)
  zapi.auth=JSON:decode(resp[1]).result;
  zapi.id=id
  zapi.loggedin=true;
end

--- is a wrapper function for httpPOST function
-- @param query is a fully formed  JSON RPC method call
-- @return an array, which is segmented HTTP response 
function zapi.get(query)
  if zapi.loggedin
  then
    return httpPOST(zapi.base_url,query)
  end
  return nil;
end

--- The Zabbix API call function
-- @param method is the string with the method name
-- @param params is the array with the method parameters
-- @return the JSON text on success or nil otherwise
-- @usage zapi.call("item.get")
-- @usage zapi.call("item.get,{["host"]=123})
function zapi.call(method,params)
  if not method
  then
    error("zapi.call(): method is a mandatory argument")
    return nil
  end
  
  if zapi.loggedin and method
  then
    local theQuery={}
    theQuery["jsonrpc"]="2.0"
    theQuery["method"]=method
    theQuery["params"]={}
    if zapi.extendout
    then
      theQuery["params"]["output"]="extend"
    end
    if params
    then
      for k,v in pairs(params)
      do
        theQuery["params"][k]=v
      end
    end
    theQuery["auth"]=zapi.auth
    theQuery["id"]=zapi.id
    local jsonQuery=JSON:encode_pretty(theQuery)
    zapi.trace(jsonQuery)
    return zapi.jtr(httpPOST(zapi.base_url,jsonQuery))
  end
  error("zapi.call(): not logged in")
  return nil
end
--- The httpPOST function forms the HTTP POST
-- request and sends this to the server
-- @param base_url is the URL of the Zabbix JSON RPC API
-- @param query is the JSON method call with params
-- @return the array with the result of the RPC call
function httpPOST(base_url,query)
  if not (base_url and query)
  then
      error("Get(): base_url and query are mandatory arguments")
      return nil
  end
  local response_body={}
  zapi.socket.http.request{
    url=base_url,
    method="POST",
    headers = {
      ["Accept-Encoding"] = "gzip, deflate",
      ["Content-Type"] = "application/json-rpc",
      ["Content-Length"] = string.len(query)
    },
    source = zapi.ltn12.source.string(query),
    sink = zapi.ltn12.sink.table(response_body)
  }
  return response_body;
end

--- joins the HTTP response array into a string
-- @param text_array - the HTTP response array
-- @return string
function zapi.jtr(text_array)
  if not text_array
  then
    error("jtr(): text_array is a mandatory argument, with a contents of a HTTP response array")
    return nil
  end
  local ret=""
  for i = 1,#text_array
  do
    ret=ret..text_array[i];
  end
  return ret;
end

return zapi
