-- vim: ts=4 tabstop=4 shiftwidth=4 expandtab
-- vim: retab 
-- JSON-Lua test script for the JSON parser from Jeffry Friedl
-- from http://regex.info/blog/lua/json
--
-- JSON text is a data format description for MMCP protocol
-- MMCP - Multicast Monitoring and Control Protocol (c) KBPS
-- Author: Pavel Kraynyukhov
-- 
package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";

local MCASTMUX_HOME=os.getenv("MCASTMUX_HOME");

local lscript_path=MCASTMUX_HOME.."/lua"

local JSONf = assert(loadfile(lscript_path.."/JSON.lua"));

JSON=JSONf()

local socket = require("socket")
local ltn12 = require("ltn12")
local http = require("socket.http")
local url = require("socket.url")
local xml = require("LuaXml")


-- local base_url="http://10.244.244.103/zabbix/api_jsonrpc.php"
local base_url="http://10.244.244.74/zabbix/api_jsonrpc.php"

local login= [[
{
  "jsonrpc":"2.0",
  "method":"user.authenticate",
  "params":{
    "user":"admin",
    "password":"123"
  },
  "id": 1
}
]]

--local login= [[
--{
--  "jsonrpc":"2.0",
--  "method":"user.authenticate",
--  "params":{
--    "user":"noc",
--    "password":"J2PmZ98eI4cyNs6Mo7cu"
--  },
--  "id": 1
--}
--]]

-- local packet_format = JSON:decode(packet_format_json);

--print(packet_format.header.BRID.bits, packet_format.header.BRID.type);

function SOAPcreateCard(user,password,className,attributesList)
  local soapMessage=[[<?xml version="1.0"?>
  <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://soap.services.cmdbuild.org">
    <SOAP-ENV:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
      <ns1:createCard>
        <ns1:cardType>
          <ns1:className>]]..className..[[</ns1:className>]]

  for k,v in pairs(attributesList)
  do
    soapMessage=soapMessage..[[

            <ns1:attributeList>
              <ns1:name>]]..k..[[</ns1:name>
              <ns1:value>]]..v..[[</ns1:value>
            </ns1:attributeList>]]
  end
  soapMessage=soapMessage..[[

          <ns1:beginDate>]]..os.date("%Y-%m-%dT%H:%M:%SZ")..[[</ns1:beginDate>
          <ns1:user>]]..user..[[</ns1:user>
        </ns1:cardType>
      </ns1:createCard>
    </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>]]
  return soapMessage;
end

function getFieldsNr()
    if packet_format.body.tripplet
    then
        return  #packet_format.body.tripplet
    else
        return 0;
    end
end

function getFieldSize(idx)
    if packet_format.body.tripplet
    then
        return packet_format.body.tripplet[idx].bytes;
    end
end

function getFieldType(idx)
    if packet_format.body.tripplet
    then
        return packet_format.body.tripplet[idx].type;
    end
end

function exportBodyFormat()
    for i = 1,#packet_format.body.tripplet
    do
      addFieldMapping(packet_format.body.tripplet[i].bytes, packet_format.body.tripplet[i].type);
    end
end

function soapSend(tURL,query)
  local response_body={}
  socket.http.request{
    url=tURL,
    method="POST",
    headers = {
      ["Content-Type"] = "text/xml;charset=UTF-8",
      ["SOAPAction"] = "",
      ["Content-Length"] = string.len(query)
    },
    source = ltn12.source.string(query),
    sink = ltn12.sink.table(response_body)
  }
  return response_body;
end

function zGet(query)
  local response_body={}
  socket.http.request{
    url=base_url,
    method="POST",
    headers = {
      ["Content-Type"] = "application/json-rpc",
      ["Content-Length"] = string.len(query)
    },
    source = ltn12.source.string(query),
    sink = ltn12.sink.table(response_body)
  }
  return response_body;
end

function joinTheResponse(text_array)
    local ret=""
    for i = 1,#text_array
    do
      ret=ret..text_array[i];
    end
    return ret;
end

function mkQuery(method, auth, id, ...)
  local theQuery=[[ {
    "jsonrpc": "2.0",
    "method": "]]..method..[[",
    "params" : {
      "output":"extend"
  ]]

  for i,v in ipairs(arg) do
    theQuery=theQuery..","..v;
  end
  theQuery=theQuery..[[}, "auth" : "]]..auth..[[", "id" : ]]..id .."}";
  return theQuery;
end

function mkQueryPrecise(method, auth, id, ...)
  local theQuery=[[ {
    "jsonrpc": "2.0",
    "method": "]]..method..[[",
    "params" : {
  ]]

  for i,v in ipairs(arg) do
    if i == 1
    then 
      theQuery=theQuery..v;
    else
    theQuery=theQuery..",\n"..v;
    end
  end
  theQuery=theQuery.."\n  "..[[}, "auth" : "]]..auth..[[", "id" : ]]..id .."\n }";
  return theQuery;
end

function main()


  local response=zGet(login);

  local resp=JSON:decode(response[1]);
  local auth=resp.result;


  response={};

--[[  response=zGet(mkQuery("host.get",auth,1));

  resp=JSON:decode(joinTheResponse(response)); 

  local back2JSON = JSON:encode_pretty(resp);

  print("========================== HOSTS ==========================");

  print(back2JSON);
  ]]--

--  response={};

--  response=zGet(mkQuery("hostinterface.get",auth,1));

--  resp=JSON:decode(joinTheResponse(response)); 

--  back2JSON = JSON:encode_pretty(resp);

--  print("========================== INTERFACES ==========================");

--  print(back2JSON);

--[[

  for i = 1,#resp.result
  do
    print("INTERFACEID: " .. resp.result[i].interfaceid)
    print("HOSTID: " .. resp.result[i].hostid)
    print("IP: " .. resp.result[i].ip)
  end

]]--

--[[  response=zGet(mkQuery("usermacro.get",auth,1));

  resp=JSON:decode(joinTheResponse(response));

  back2JSON = JSON:encode_pretty(resp);

  print("========================== MACRO ==========================");

  print(back2JSON);

  local param={}

  param["globalmacro"]="true"
]]--

--  local qury=mkQueryPrecise("usermacro.get",auth,1,[["output": "extend"]],[["globalmacro":true]]);

--[[
  response=zGet(qury);

  resp=JSON:decode(joinTheResponse(response));

  back2JSON = JSON:encode_pretty(resp);

  print("========================== GLOBAL MACRO ==========================");

  print(back2JSON);
]]--

  response=zGet(mkQuery("item.get",auth,1));

  resp=JSON:decode(joinTheResponse(response));

  back2JSON = JSON:encode_pretty(resp);

  print("========================== ITEMS ==========================");

  print(back2JSON);

--  print("========================== KEYS ==========================");
--  for i=1,#resp.result
--  do
--    if(resp.result[i].name == "Ошибки аутентификации")
--    then
--      print("key: "..resp.result[i].key_);
--    end
--  end

--  response=zGet(mkQuery("trigger.get",auth,1));

--  local resp=joinTheResponse(response);
--  local back2JSON = JSON:encode_pretty(resp);

--  print("========================== TRIGGERS ==========================");

--  print(back2JSON);

end


function getObjects(auth,host, ...)
  local query=mkQueryPrecise("items.getobjects",auth,1,[[  "host" : "]]..host..[["]]);
  print(query);
  local response=zGet(query);
  local resp=JSON:decode(joinTheResponse(response));
--  local back2JSON = JSON:encode_pretty(resp);
--  print(back2JSON)
  return resp
end

function getHosts(auth)
  local response=zGet(mkQuery("host.get",auth,1));
  local resp=JSON:decode(joinTheResponse(response));
--  local back2JSON = JSON:encode_pretty(resp);
--  print(back2JSON)
  return resp.result
end


function test()
  local response=zGet(login);

  local resp=JSON:decode(response[1]);
  local auth=resp.result;

  resp={}
  resp=getHosts(auth);
  if resp ~= nil
  then
    for i=1,#resp 
    do
     table.foreach(resp[i],print);
     if resp[i].host ~= nil
     then
       resp=getObjects(auth,resp[i].host);
       table.foreach(resp,print);
     end
    end
  end
end

--test()






function soapTEST()
  ptabkv()

  local attributes = {};
  local response={}

  for key,val in pairs(tabkv)
  do
    attributes["Description"]=key
    attributes["zStorage"]=val
    response=soapSend("http://10.244.244.150:8080/cmdbuild/services/soap/Webservices",SOAPcreateCard("admin","admin","zkey_",attributes));
  end
--  print(response)
end

--soapTEST()


main()
