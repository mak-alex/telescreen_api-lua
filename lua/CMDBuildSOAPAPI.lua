-- vim: set ts=4 tabstop=4 shiftwidth=4 expandtab
-- vim: retab 
-- Author: Pavel Kraynyukhov
-- 
package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua";

local socket = require("socket")
local ltn12 = require("ltn12")
local http = require("socket.http")
local url = require("socket.url")

require("LuaXml")
local XML = xml

local SOAPMessage={}

function SOAPMessage.createCard(user,password,className,attributesList,metadata)
if not (user and password and className)
then
    error("createCard(): Mandatory attributes: user, password, className");
    return nil
end
local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap1="http://soap.services.cmdbuild.org">
    <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
    </soap:Header>
    <soap:Body>
        <soap1:createCard>
            <soap1:cardType>
                <soap1:className>]]..className..[[</soap1:className>
]]
    for k,v in pairs(attributesList)
    do
        soapMessage=soapMessage..[[
                <soap1:attributeList>
                      <soap1:name>]]..k..[[</soap1:name>
                      <soap1:value>]]..v..[[</soap1:value>
                </soap1:attributeList>
]]
    end
    if (metadata)
    then
        soapMessage=soapMessage..[[
                <soap1:metadata>
                      <soap1:key>]]..metadata["key"]..[[</soap1:key>
                      <soap1:value>]]..metadata["value"]..[[</soap1:value>
                 </soap1:metadata>
]]
    end
    soapMessage=soapMessage..[[
            </soap1:cardType>
        </soap1:createCard>
    </soap:Body>
</soap:Envelope>
]]
    return soapMessage;
end

function SOAPMessage.deleteCard(user,password,className,cardId)
if not (user and password and className and cardId)
then
    error("deleteCard(): Mandatory attributes: user, password, className, cardId");
    return nil
end
local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap1="http://soap.services.cmdbuild.org">
    <soap:Header>
        <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
              <wsse:Username>]]..user..[[</wsse:Username>
              <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
            </wsse:UsernameToken>
          </wsse:Security>
    </soap:Header>
    <soap:Body>
        <soap1:deleteCard>
          <soap1:className>]]..className..[[</soap1:className>
          <soap1:cardId>]]..cardId..[[</soap1:cardId>
      </soap1:deleteCard>
    </soap:Body>
</soap:Envelope>
]]
  return soapMessage;
end

function SOAPMessage.updateCard(user,password,className,cardId,attributesList,metadata)
if not (user and password and className and cardId)
then
    error("updateCard(): Mandatory attributes: user, password, className, cardId");
    return nil
end
local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap1="http://soap.services.cmdbuild.org">
    <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
    </soap:Header>
    <soap:Body>
        <soap1:updateCard>
            <soap1:card>
                <soap1:className>]]..className..[[</soap1:className>
                <soap1:Id>]]..cardId..[[</soap1:Id>
]]
    for k,v in pairs(attributesList)
    do
        soapMessage=soapMessage..[[
                <soap1:attributeList>
                      <soap1:name>]]..k..[[</soap1:name>
                      <soap1:value>]]..v..[[</soap1:value>
                </soap1:attributeList>
]]
    end
    if (metadata)
    then
        soapMessage=soapMessage..[[
                <soap1:metadata>
                      <soap1:key>]]..metadata["key"]..[[</soap1:key>
                      <soap1:value>]]..metadata["value"]..[[</soap1:value>
                </soap1:metadata>
]]
    end
    soapMessage=soapMessage..[[
            </soap1:card>
        </soap1:updateCard>
    </soap:Body>
    </soap:Envelope>
]]
return soapMessage;
end

function SOAPMessage.getCard(user,password,className,cardId,attributeList)
if not (user and password and className and cardId)
then
    error("getCard(): Mandatory attributes: user, password, className, cardId");
    return nil
end
local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://soap.services.cmdbuild.org">
   <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <soap1:getCard>
]]
    if(className)
    then
        soapMessage=soapMessage..[[
         <soap1:className>]]..className..[[</soap1:className>
]]
    end
    soapMessage=soapMessage..[[
         <soap1:cardId>]]..cardId..[[</soap1:cardId>
]]
    if(attributeList)
    then
        for k,v in pairs(attributeList)
        do
        soapMessage=soapMessage..[[
                <soap1:attributeList>
                      <soap1:name>]]..k..[[</soap1:name>
                      <soap1:value>]]..v..[[</soap1:value>
                </soap1:attributeList>
]]
        end
    end
    soapMessage=soapMessage..[[
      </soap1:getCard>
   </soap:Body>
</soap:Envelope>
]]
return soapMessage;
end

function SOAPMessage.getCardList(user,password,className,attributeList,filter,filterSQOperator,orderType,limit,offset,fullTextQuery,cqlQuery,cqlQueryParameters)
if not (user and password and className)
then
    error("getCardList(): Mandatory attributes: user, password, className");
    return nil
end
local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://soap.services.cmdbuild.org">
   <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <soap1:getCardList>
         <soap1:className>]]..className..[[</soap1:className>
]]
    if(attributeList)
    then
        for k,v in pairs(attributeList)
        do
        soapMessage=soapMessage..[[

            <soap1:attributeList>
                  <soap1:name>]]..k..[[</soap1:name>
                  <soap1:value>]]..v..[[</soap1:value>
            </soap1:attributeList>]]
        end
    end
    if( filter or filterSQOperator)
    then
        soapMessage=soapMessage..[[
         <soap1:queryType>
]]
        if(filter)
        then
            soapMessage=soapMessage..[[
            <soap1:filter>
               <soap1:name>]]..filter["name"]..[[</soap1:name>
               <soap1:operator>]]..filter["operator"]..[[</soap1:operator>
               <soap1:value>]]..filter["value"]..[[</soap1:value>
            </soap1:filter>
]]
        end
        if(filterSQOperator)
        then
            soapMessage=soapMessage..[[
            <soap1:filterOperator>
]]
             for i=1,table.maxn(filterSQOperator)
             do
                soapMessage=soapMessage..[[
                <soap1:operator>]]..filterSQOperator[i]["operator"]..[[</soap1:operator>
                <soap1:subquery>
                    <soap1:filter>
                        <soap1:name>]]..filterSQOperator[i]["subquery"]["name"]..[[</soap1:name>
                        <soap1:operator>]]..filterSQOperator[i]["subquery"]["operator"]..[[</soap1:operator>
                        <soap1:value>]]..filterSQOperator[i]["subquery"]["value"]..[[</soap1:value>
                    </soap1:filter>
                </soap1:subquery>
]]
             end
            soapMessage=soapMessage..[[
            </soap1:filterOperator>
]]
         end
         soapMessage=soapMessage..[[
         </soap1:queryType>
]]
     end
     if(orderType)
     then
        soapMessage=soapMessage..[[
         <soap1:orderType>
            <soap1:columnName>]]..orderType["columnName"]..[[</soap1:columnName>
            <soap1:type>]]..orderType["type"]..[[</soap1:type>
         </soap1:orderType>
]]
     end
     if(limit)
     then
        soapMessage=soapMessage..[[
         <soap1:limit>]]..limit..[[</soap1:limit>
]]
     end
     if(offset)
     then
        soapMessage=soapMessage..[[
         <soap1:offset>]]..offset..[[</soap1:offset>
]]
     end
     if(fullTextQuery)
     then
        soapMessage=soapMessage..[[
         <soap1:fullTextQuery>]]..fullTextQuery..[[</soap1:fullTextQuery>
]]
     end
     if(cqlQuery)
     then
        soapMessage=soapMessage..[[
         <soap1:cqlQuery>
            <soap1:cqlQuery>]]..cqlQuery..[[</soap1:cqlQuery>
]]
        if(cqlQueryParameters)
        then
            soapMessage=soapMessage..[[
            <soap1:parameters>
               <soap1:key>]]..cqlQueryParameters["key"]..[[</soap1:key>
               <soap1:value>]]..cqlQueryParameters["value"]..[[</soap1:value>
            </soap1:parameters>
]]
         end
        soapMessage=soapMessage..[[
         </soap1:cqlQuery>
]]
     end
     soapMessage=soapMessage..[[

      </soap1:getCardList>
   </soap:Body>
</soap:Envelope>
]]
    return soapMessage
end

function SOAPMessage.createLookUp(user,password,lookUpType,code,description,Id,notes,parentId,position)
if not (user and password and lookUpType and code and description)
then
    error("createLookUp(): Mandatory attributes: user, password, lookUpType, code, description");
    return nil
end

local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://soap.services.cmdbuild.org">
   <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <soap1:createLookup>
         <soap1:lookup>
            <soap1:code>]]..code..[[</soap1:code>
            <soap1:description>]]..description..[[</soap1:description>
]]
    if(Id)
    then
        soapMessage=soapMessage..[[
            <soap1:id>]]..Id..[[</soap1:id>
]]
    end
    if(notes)
    then
        soapMessage=soapMessage..[[
            <soap1:notes>]]..notes..[[</soap1:notes>
]]
    end
    if(parentId and position)
    then
        soapMessage=soapMessage..[[
            <soap1:parent/>
            <soap1:parentId>]]..parentId..[[</soap1:parentId>
            <soap1:position>]]..position..[[</soap1:position>
]]
    end
    soapMessage=soapMessage..[[
            <soap1:type>]]..lookUpType..[[</soap1:type>
         </soap1:lookup>
      </soap1:createLookup>
   </soap:Body>
</soap:Envelope>
]]
    return soapMessage;
end

function SOAPMessage.getLookupList(user,password,lookUpType,value,needParentList)
if not (user and password and lookUpType)
then
    error("getLookupList(): Mandatory attributes: user, password, lookUpType")
    return nil
end

local soapMessage=[[<?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:soap1="http://soap.services.cmdbuild.org">
   <soap:Header>
      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
          <wsse:Username>]]..user..[[</wsse:Username>
          <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">]]..password..[[</wsse:Password>
        </wsse:UsernameToken>
      </wsse:Security>
   </soap:Header>
   <soap:Body>
      <soap1:getLookupList>
         <soap1:type>]]..lookUpType..[[</soap1:type>
]]
    if value
    then
        soapMessage=soapMessage .. [[
         <soap1:value>]]..value..[[</soap1:value>
]]
    end
    if needParentList and needParentList == true
    then
        soapMessage=soapMessage .. [[
         <soap1:parentList>]]..true..[[</soap1:parentList>
]]
    end
    soapMessage=soapMessage .. [[
      </soap1:getLookupList>
   </soap:Body>
</soap:Envelope>
]]
    return soapMessage
end

function SOAPMessage.Send(tURL,query)
  local response_body={}
  socket.http.request{
    url=tURL,
    method="POST",
    headers = {
      ["Content-Type"] = "text/xml;charset=UTF-8",
      ["SOAPAction"] = "",
      ["Content-Length"] = string.len(query)
    },
    source = ltn12.source.string(query),
    sink = ltn12.sink.table(response_body)
  }
  return response_body;
end

function SOAPMessage.retriveMessage(response)
    local resp=SOAPMessage.jtr(response)
    local istart,iend = resp:find('<soap:Envelope.*</soap:Envelope>');
    if(istart and iend)
    then
        return resp:sub(istart,iend);
    else
        return nil
    end
end

function SOAPMessage.jtr(text_array)
    local ret=""
    for i = 1,#text_array
    do
      ret=ret..text_array[i];
    end
    return ret;
end

function parseCardsList(xmltab)
    local result=xmltab:find("ns2:return") --xmltab[1][1][1];
    local rows=xmltab:find("ns2:totalRows")[1] 
--    local className=xmltab:find("ns2:className")[1]
    local luatab={}
    for i=1,rows
    do
        local card = result[i]:find("ns2:cards");
        local cardId=tostring(card:find("ns2:id")[1]);
        for j=0,table.maxn(card)
        do
            if(XML.tag(card[j]) == "ns2:attributeList")
            then
                local attrName=card[j]:find("ns2:name")[1]
                local theValue=card[j]:find("ns2:value")[1]
                if( attrName and theValue)
                then
                    local subtab = {}
                    subtab[attrName]=theValue;
                    luatab[cardId] = subtab; 
                end
            end
        end
    end
    return luatab
end

return SOAPMessage
