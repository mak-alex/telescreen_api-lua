local socket                          = require("socket")
local http                            = require("socket.http")
local ftp                             = require("socket.ftp")
local url                             = require("socket.url")
local ltn12                           = require("ltn12")
                                        require("logging")
                                        require("logging.file")
local logger                          = logging.file("telescreen%s.log","%Y-%m-%d")
-- returns a string with the current state of the download
local remaining_s                     = "Cached in JSON file (TeleSCREEN): \t%s received, %s/s throughput, %2.0f%% done, %s remaining %s"
local elapsed_s                       = "Cached in JSON file (TeleSCREEN): \t%s received, %s/s throughput, %s elapsed %s           "
WGET={
    ["AboutME"]={
          _MODULE         = "WGET",
          _VERSION        = "WGET v0.1.0-rc1",
          _AUTHOR         = "Alexandr Mikhailenko (he also) FlashHacker",
          _URL            = "https://bitbucket.org/enlab/WGET",
          _MAIL           = "flashhacker1988@gmail.com",
          _COPYRIGHT      = "Design Bureau of Industrial Communications LTD, 2015. All rights reserved.",
          _LICENSE        = [[
            MIT LICENSE
            Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex pseudoclass.A.K
            Permission is hereby granted, free of charge, to any person obtaining a
            copy of this software and associated documentation files (the
            "Software"), to deal in the Software without restriction, including
            without limitation the rights to use, copy, modify, merge, publish,
            distribute, sublicense, and/or sell copies of the Software, and to
            permit persons to whom the Software is furnished to do so, subject to
            the following conditions:
            The above copyright notice and this permission notice shall be included
            in all copies or substantial portions of the Software.
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
            OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
            MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
            IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
            CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
            TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
            SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
            ]],
          _DESCRIPTION    = [[
            Caching http://*.json to file....
            Dependencies:
                logging # luarocks install logging --local
            How to use:
                local WGET                    = require("wget")
                WGET.get(TeleSCREEN_URL, "status.json")
          ]]
    }, ["nicetime"]=function(s) -- formats a number of seconds into human readable form
        local l = "s"
        if s > 60 then s = s / 60 l = "m"if s > 60 then s = s / 60 l = "h" if s > 24 then s = s / 24 l = "d"end end end
        if l == "s" then return string.format("%5.0f%s", s, l) else return string.format("%5.2f%s", s, l) end
    end, ["nicesize"]=function(b) -- formats a number of bytes into human readable form
        local l = "B"
        if b > 1024 then b = b / 1024 l = "KB"if b > 1024 then b = b / 1024 l = "MB"if b > 1024 then b = b / 1024 l = "GB" end end end
        return string.format("%7.2f%2s", b, l)
    end, ["gauge"]=function(got, delta, size)
        local rate = got / delta
        if size and size >= 1 then
            logger:info(string.format(remaining_s, WGET.nicesize(got), WGET.nicesize(rate), 100*got/size, WGET.nicetime((size-got)/rate), " - done!"))
            return string.format(remaining_s, WGET.nicesize(got), WGET.nicesize(rate), 100*got/size, WGET.nicetime((size-got)/rate), " - done!")
        else
            logger:info(string.format(elapsed_s, WGET.nicesize(got), WGET.nicesize(rate), WGET.nicetime(delta), " - done!"))
            return string.format(elapsed_s, WGET.nicesize(got), WGET.nicesize(rate), WGET.nicetime(delta), " - done!")
        end
    end, ["stats"]=function(size)       -- creates a new instance of a receive_cb that saves to disk
                                        -- kind of copied from luasocket's manual callback examples
        local start = socket.gettime()
        local last = start
        local got = 0
        return function(chunk)
            -- elapsed time since start
            local current = socket.gettime()
            if chunk then
                -- total bytes received
                got = got + string.len(chunk)   
                -- not enough time for estimate
                if current - last > 1 then
                    io.stderr:write("\r", WGET.gauge(got, current - start, size))
                    io.stderr:flush()
                    last = current
                end
            else
                -- close up
                io.stderr:write("\r", WGET.gauge(got, current - start), "\n")
            end
            return chunk
        end
    end, ["gethttpsize"]=function(u)     -- determines the size of a http file
        local r, c, h = http.request {method = "HEAD", url = u}
        if c == 200 then
            return tonumber(h["content-length"])
        end
    end, ["getbyhttp"]=function(u, file) -- downloads a file using the http protocol
        local save = ltn12.sink.file(file or io.stdout)
        -- only print feedback if output is not stdout
        if file then save = ltn12.sink.chain(WGET.stats(WGET.gethttpsize(u)), save) end
        local r, c, h, s = http.request {url = u, sink = save }
        if c ~= 200 then io.stderr:write(s or c, "\n") logger:error(s or c, "\n") end
    end, ["getbyftp"]=function(u, file) -- downloads a file using the ftp protocol
        local save = ltn12.sink.file(file or io.stdout)
        -- only print feedback if output is not stdout
        -- and we don't know how big the file is
        if file then save = ltn12.sink.chain(stats(), save) end
        local gett = url.parse(u)
        gett.sink = save
        gett.type = "i"
        local ret, err = ftp.get(gett)
        if err then print(err) logger:error(err) end
    end, ["getscheme"]=function(u) -- determines the scheme
        -- this is an heuristic to solve a common invalid url poblem
        if not string.find(u, "//") then u = "//" .. u end
        local parsed = url.parse(u, {scheme = "http"})
        return parsed.scheme
    end, ["get"]=function(u, name) -- gets a file either by http or ftp, saving as <name>
        local fout = name and io.open(name, "wb")
        local scheme = WGET.getscheme(u)
        if scheme == "ftp" then WGET.getbyftp(u, fout)
        elseif scheme == "http" then WGET.getbyhttp(u, fout)
        else print("unknown scheme" .. scheme) logger:error("unknown scheme" .. scheme) end
    end   
}

return WGET